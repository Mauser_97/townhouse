<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Registro</title>
    <link rel="stylesheet" href="css/registro.css">
    <link rel="stylesheet" href="./css/style.css">
    <link rel="shortcut icon" href="img/logo.ico" />

</head>
<body class="backcolor">
  
    <form class="form-registrar">
    <a href="menu.html">
       <img src="img/logo.png" class="logo" >
       </a>
        <h2 class="form-titulo">Crea una Cuenta Nueva</h2>
        <div class="div-registro">
            <input type="text" name="nombre" placeholder="Nombre" required="" class="input-50">
            <input type="text" name="apellido" placeholder="Apellido" required="" class="input-50">
            <input type="email" name="correo" placeholder="Correo" required="" class="input-100">
            <input type="text" name="usuario" placeholder="usuario" required="" class="input-50">
            <input type="password" name="clave" placeholder="contraseña" required="" class="input-50">
            <input type="text" name="telefono" placeholder="telefono" required="" class="input-100">
            <input type="submit" value="Registrar" class="btn-enviar">
            <p class="link-login"> Ya tienes una cuenta?  <a href="login.php">Ingresa  Aqui!</a> </p>
        </div>
    </form>
</body>
</html>