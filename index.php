<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>The Town House</title>
  <link rel="stylesheet" href="./css/style.css">
  <link rel="stylesheet" href="./css/flexboxgrid-6.3.1/css/flexboxgrid.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body class="backcolor">
  <div class="row main-container center-xs">
    <div class="col-md-9 col-sm-10 col-xs-11">
      <div class="box">
        <div class="card animated fadeInUp">
          <!--******************************** Menu de navegacion ***************************** -->
          <nav class="navbar navbar-expand-lg navbar-light bg-light footerandHeader">
          <a href="./index.php"> <img src="img/logo.png" class="logomin"> </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="./index.php">Inicio<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Quienes Somos <span class="sr-only">(current)</span></a>
                </li>
              </ul>
              <form class="form-inline my-2 my-lg-0">
                <label class="form-control mr-sm-2" for="">Nombre de Usuario</label>
                <a class="btn btn-primary" href="./login.php" role="button">Iniciar Sesion</a>
              </form>
            </div>
          </nav>
          <!--******************************** FIN Menu de navegacion ***************************** -->
        </div>
      </div>
      <div style="margin-top:45px">
      </div>
      <div  class="">      
        <div class="ship_list body  bordes fondoen">
          <header class="text-center">
           <h1 class="tittle threeD margenes">THE TOWN HOUSE</h1>
          </header>
         <div class="ship">
        <div class="row center-xs">
          <div class="col-xs-9 col-sm-4 col-md-2  flags animated fadeInDown delay-1">
            <div class="cards margenes">
              <a href="./vista/gestionar/gestionar.php"><img src="./img/gestionar.png" alt="gestionar img"></a>
              <h3 class="cardtext">Gestionar</h3>
            </div>
          </div>
          <div class="col-xs-9 col-sm-4 col-md-2  flags animated fadeInDown delay-2">
            <div class="cards margenes">
              <a href="./vista/operaciones/operaciones.php"><img src="./img/operaciones.png" alt="operaciones img"></a>
              <h3 class="cardtext">Operaciones</h3>
            </div>
          </div>
          <div class="col-xs-9 col-sm-4 col-md-2  flags animated fadeInDown delay-3">
            <div class="cards margenes">
              <a href="./vista/reportes/reportes.php"><img src="./img/reporte.png" alt="reporte img"></a>
              <h3 class="cardtext">Reportes</h3>
            </div>
          </div>
          <div class="col-xs-9 col-sm-4 col-md-2  flags animated fadeInDown delay-3">
            <div class="cards margenes">
              <a href="./vista/configuraciones/configuraciones.php"><img src="./img/configuracion.png" alt="configuraciones img"></a>
              <h3 class="cardtext">Configuración</h3>
            </div>
          </div>
        </div>
      </div>
      </div>
      </div>
      <div style="margin-bottom:35px">
      </div>
    </div>
  </div>
  <!--******************************FOOTER************************************************-->
  <div class="card">
    <div class="card-body">
      <blockquote class="blockquote mb-0">
        <p>Contactanos</p>
        <footer class="blockquote-footer">Teléfono:  <cite title="Source Title">+505 88997744</cite></footer>
      </blockquote>
    </div>
  </div>
  <!--**********************************FIN DEL FOOTER*******************************************-->
</body>

</html>