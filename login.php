<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/Login.css">
    <link rel="stylesheet" href="./css/style.css">
    <link rel="shortcut icon" href="img/logo.ico" />

	<title>Login</title>
</head>
<body class="backcolor">

<div class="cuadrado ">
	
   
  <form class="form-login">
  <h2 class="form-titulo">Iniciar sesion</h2>
   <a href="./index.php"> <img src="img/logo.png" class="logo"> </a>

   <input type="text" name="usuario" required="" placeholder="usuario"  class="input-log">
   <input type="password" name="clave" required="" placeholder="contraseña" class="input-log">
  <input type="submit" name="login"  value="Login" class="btn-log">
  <h4>o</h4>
  <a class="btn-log anchor" href="./registro.php" >Registrate</a>
  </form>
</div>
</body>
</html>