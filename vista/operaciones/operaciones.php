<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>The Town House</title>
    <link rel="stylesheet" href="../.././css/style.css">
    <link rel="stylesheet" href="../.././css/flexboxgrid-6.3.1/css/flexboxgrid.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body class="backcolor">
    <div class="row main-container center-xs">
      <div class="col-md-9 col-sm-10 col-xs-11">
        <div class="box">
        <div class="card animated fadeInUp">
          <!--******************************** Menu de navegacion ***************************** -->
          <nav class="navbar navbar-expand-lg navbar-light bg-light footerandHeader bordes">
          <a class="navbar-brand" href="../.././index.php">TOWN HOUSE</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="../../index.php">Inicio<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="#">Quienes somos <span class="sr-only">(current)</span></a>
                </li>
              </ul>
              <form class="form-inline my-2 my-lg-0">
                <label class="form-control mr-sm-2" for="">Nombre de Usuario</label>
                <a class="btn btn-primary" href="" role="button">Iniciar Sesion</a>
              </form>
            </div>
          </nav>
           <!--******************************** FIN Menu de navegacion ***************************** -->
          </div>
          </div>
          <div style="margin-top:45px">
          </div>
          <!--*****************************************BODY ******************************************-->
          <div class="">
              <div class="ship_list body  bordes fondoen">
                <header class="text-center  ">
                  <h1 class="tittle threeD margenes">OPERACIONES</h1>
                </header>
                <div class="ship" >
                  <div class="row center-xs">
                    <div class="col-xs-9 col-sm-4 col-md-2  flags animated fadeInDown delay-1">
                    <div class="cards margenes" >
                        <a href="gestionar_pago.php"><img src="./img/pago.png" alt="Image pago"></a>
                        <h3 class="cardtext">Gestionar Pagos</h3>
                      </div>
                    </div>
                    <div class="col-xs-9 col-sm-4 col-md-2  flags animated fadeInDown delay-2">
                    
                    <div class="cards margenes" >
                        <a href="reservacion.php"><img src="./img/reserva.png" alt="Image reservacion"></a>
                        <h3 class="cardtext">Gestionar Reserva</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--******************************FIN DEL BODY***********************************************-->
        </div>
        </div>
      </div>
    </div>
  <!--******************************FOOTER************************************************-->
  <div class="card">
    <div class="card-body">
      <blockquote class="blockquote mb-0">
        <p>Contactanos</p>
        <footer class="blockquote-footer">Teléfono:  <cite title="Source Title">+505 88997744</cite></footer>
      </blockquote>
    </div>
  </div>
  <!--**********************************FIN DEL FOOTER*******************************************-->
  </body>
</html>
