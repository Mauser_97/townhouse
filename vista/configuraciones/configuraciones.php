<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>The Town House</title>
    <link rel="stylesheet" href=".././css/style.css">
    <link rel="stylesheet" href=".././css/flexboxgrid-6.3.1/css/flexboxgrid.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
  </head>
  <body class="backcolor">
    <div class="row main-container center-xs">
      <div class="col-md-9 col-sm-10 col-xs-11">
        <div class="box">
        <div class="card animated fadeInUp">
          <header class="main-header">
            <nav class="main-nav">
               <img src=".././img/logo.png" width="25"alt="Logo">
               <a href="../index.php" class="nav-link">Inicio</a>
            </nav>
          </header>
          </div>
          </div>
          <div class="ship_list body">
            <header class="text-center">
              <img src="./img/logo.png" height="115" alt="Logo de town house">
              <h1 class="tittle threeD">GESTIONAR</h1>
            </header>
          </div>
            <div class="ship">
              <div class="row center-xs">
                <div class="col-xs-10 col-sm-6 col-md-4 flags animated fadeInDown delay-1">
                <a href="gestionar_usuario.php"><img src="./img/usuario.png" alt="Image usuarios"></a>
                <div class="card">
                    <h3>Gestionar Usuario</h3>
                  </div>
                </div>
                <div class="col-xs-10 col-sm-6 col-md-4 flags animated fadeInDown delay-1">
                <a href="idioma.php"><img src="./img/idioma.png" alt="Image idiomas"></a>
                <div class="card">
                    <h3>Cambiar Idioma</h3>
                  </div>
                </div>
            </div>
        </div>
        </div>
      </div>
    </div>
  </body>
</html>
