<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>The Town House</title>
    <link rel="stylesheet" href=".././css/style.css">
    <link rel="stylesheet" href=".././css/flexboxgrid-6.3.1/css/flexboxgrid.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
  </head>
  <body class="backcolor">
    <div class="row main-container center-xs">
      <div class="col-md-9 col-sm-10 col-xs-11">
        <div class="box">
        <div class="card animated fadeInUp">
          <header class="main-header">
            <nav class="main-nav">
               <img src=".././img/logo.png" width="25"alt="Logo">
               <a href="../index.php" class="nav-link">Inicio</a>
               <a href="./configuraciones.php" class="nav-link">Regresar</a>
            </nav>
          </header>
          </div>
          </div>
          <div class="ship_list body">
            <header class="text-center">
              <img src="./img/logo.png" height="115" alt="Logo de town house">
              <h1 class="tittle threeD">CONFIGURACIONES</h1>
            </header>
          </div>
            <div class="ship_list body">
              <div class="row center-xs">
                <div class="col-xs-16 col-sm-12 col-md-8 flags animated fadeInDown delay-1">
                <div class="">
                  <div>
                  <header class="card-ventana">
                     <h4>GESTONAR USUARIOS</h4>
                   </header>
                  </div>
                    <div class="ventana"> 
                    <table height="45px">
                    <tr>
                      <td WIDTH="100px"></td>
                        <td WIDTH="100px"></td>
                        <td WIDTH="100px">Buscar por:</td>
                        <td WIDTH="100px"><select name="" id="">
                          <option value="">Seleccione</option>
                        </select></td>
                        <td WIDTH="100px"><input type="text"></td>
                        <td WIDTH="100px"><button>Buscar</button></td>
                        <td WIDTH="100px"></td>
                      <tr>
                    </table>
                    <table class="borde-tabla">
                      <tr>
                        <td  WIDTH="100px">Nombre</td>
                        <td WIDTH="100px">Apellido</td>
                        <td WIDTH="100px">Telefono</td>
                        <td WIDTH="100px">Direccion</td>
                        <td WIDTH="100px">Identificacion</td>
                        <td WIDTH="100px">Correo</td>
                        <td WIDTH="100px">Salario</td>
                      </tr>
                     </table>
                     <table height="45px">
                    <tr>
                      <td WIDTH="100px"></td>
                        <td WIDTH="100px"></td>
                        <td WIDTH="100px"></td>
                        <td WIDTH="100px"><button>Agregar</button></td>
                        <td WIDTH="100px"><button>Editar</button></td>
                        <td WIDTH="100px"><button>Eliminar</button></td>
                        <td WIDTH="100px"></td>
                      <tr>
                    </table>
                    </div>
                  </div>
                </div>
                </div>
              </div>
            </div>
        </div>
        </div>
      </div>
    </div>
  </body>
</html>
